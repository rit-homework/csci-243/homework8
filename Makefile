CFLAGS=-Wall -Wextra -std=c99 -Werror -ggdb -pedantic -pthread
VALGRIND=valgrind --error-exitcode=1 --leak-check=full --show-leak-kinds=all

all: pt-cruisers

pt-cruisers: pt-cruisers.o display.o racer.o
	$(CC) $(CFLAGS) -o $@ $^

fix-formatting:
	clang-format -i *.c *.h

clean:
	rm *.o pt-cruisers

test: pt-cruisers
	valgrind ./pt-cruisers one two three
	valgrind ./pt-cruisers 10000 one two three

.PHONY: clean fix-formatting
