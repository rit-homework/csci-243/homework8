#include "display.h"
#include "racer.h"
#include <memory.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define HELP_MESSAGE "Usage: pt-cruisers [ delay ] racer1 racer2 [racer3...]\n"

/**
 * Given a list of racer names, validates that each name is the correct length,
 * then starts and runs the racer simulation to completion.
 */
void make_racers(int count, char *names[]) {
    for (int i = 0; i < count; i++) {
        size_t len = strlen(names[i]);
        if (len > 9) {
            fprintf(stderr, "Usage: racer names must not exceed length 9.\n");
            return;
        }
    }

    pthread_t *threads = malloc(sizeof(pthread_t) * count);
    Racer **racers = malloc(sizeof(Racer) * count);

    // All Names Are Valid
    clear();
    for (int i = 0; i < count; i++) {
        Racer *racer = makeRacer(names[i], i);
        racers[i] = racer;
        pthread_create(&threads[i], NULL, run, racer);
    }

    for (int i = 0; i < count; i++) {
        pthread_join(threads[i], NULL);

        destroyRacer(racers[i]);
    }

    free(racers);
    free(threads);
}

/**
 * The main function.
 */
int main(int argc, char *argv[]) {
    if (argc < 2) {
        fprintf(stderr, HELP_MESSAGE);
        return 1;
    }

    char *first_argument = argv[1];
    int name_start_position = 2;

    char *end_ptr;
    long wait_milliseconds =
        strtoll(first_argument, &end_ptr, MAX_NAME_LEN + 1);
    if (wait_milliseconds < 1 && *end_ptr == '\0') {
        fprintf(stderr, "Delay %li is invalid.\n", wait_milliseconds);
        fprintf(stderr, HELP_MESSAGE);
        return 1;
    }

    if (*end_ptr != '\0') {
        name_start_position = 1;
        wait_milliseconds = DEFAULT_WAIT;
    }

    // Seed Random Number Generator
    srand(time(NULL));

    initRacers(wait_milliseconds);

    int count = argc - (name_start_position);
    if (count < 2) {
        fprintf(stderr, HELP_MESSAGE);
        return 1;
    }

    make_racers(count, &argv[name_start_position]);
}
