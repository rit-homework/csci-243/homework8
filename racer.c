#define _DEFAULT_SOURCE
#include "racer.h"
#include "display.h"
#include <memory.h>
#include <pthread.h>
#include <unistd.h>

static long max_wait_milliseconds;
static pthread_mutex_t mutex;

void initRacers(long milliseconds) {
    max_wait_milliseconds = milliseconds;
    int ret = pthread_mutex_init(&mutex, NULL);
    if (ret != 0) {
        fprintf(stderr, "Failed to Initialize Mutex\n");
        exit(1);
    }
}

char *pad_string(char *unpadded) {
    char *padded = malloc(sizeof(char) * (MAX_NAME_LEN + 1));
    if (padded == NULL) {
        return NULL;
    }

    size_t unpaddedLength = strnlen(unpadded, MAX_NAME_LEN);
    size_t neededPadding = (MAX_NAME_LEN - unpaddedLength) / 2;
    for (unsigned int i = 0; i < neededPadding; i++) {
        padded[i] = '_';
    }

    strcpy(&padded[neededPadding], unpadded);

    size_t rightStart = neededPadding + unpaddedLength;
    if (neededPadding * 2 + unpaddedLength != MAX_NAME_LEN) {
        padded[rightStart] = '_';
        rightStart++;
    }

    for (; rightStart < MAX_NAME_LEN; rightStart++) {
        padded[rightStart] = '_';
    }

    padded[rightStart] = '\0';
    return padded;
}

Racer *makeRacer(char *name, int position) {
    Racer *racer = malloc(sizeof(Racer));
    if (racer == NULL) {
        return NULL;
    }

    racer->dist = 0;
    racer->row = position;

    racer->graphic = pad_string(name);
    if (racer->graphic == NULL) {
        return NULL;
    }

    return racer;
}

void destroyRacer(Racer *racer) {
    free(racer->graphic);
    free(racer);
}

/**
 * A typed version of run. This is called by run. It runs the racer to
 * completion.
 */
void runRacer(Racer *racer) {
    for (; racer->dist < FINISH_LINE; racer->dist++) {
        // Draw Racer
        pthread_mutex_lock(&mutex);
        set_cur_pos(1 + racer->row, 1);
        for (int i = 0; i < racer->dist; i++) {
            printf(" ");
        }

        printf("%s\n", racer->graphic);
        pthread_mutex_unlock(&mutex);

        long wait_milliseconds =
            ((1.0f * rand()) / RAND_MAX) * max_wait_milliseconds;
        usleep(wait_milliseconds);
    }
}

void *run(void *racer) {
    runRacer(racer);
    return NULL;
}
